# SaltStack

SaltStack ou Salt est un outil l'exécution à distance, de gestion de configuration et d'orchestration. 
SaltStack utilise le bus de données ZeroMQ pour établir une connexion chiffrée entre le Master et les Minions (Agents sur les serveurs).
SaltStack utilise le mécanisme de [Publish-Subscrive](https://fr.wikipedia.org/wiki/Publish-subscribe), pour cela, le Master ouvre le port 4505 sur lequel les Minions se rattachent, les Minions vont exécuter les commandes et envoyer le résultat de leurs travails au Salt Master sur le port 4506.

## SaltStack et ses composants

Salt contient plus de 30 modules, quelque exemple de ce qu'il est possible de faire avec des modules :

- Gérer les machines virtuelles sous OpenBSD 
- Envoyer le résultat d'exécution des Minions sur XMPP
- Utiliser JSON a la place de YAML


*Le serveur Salt*, appeler aussi «Master» est le serveur principal de SaltStack, il envoie les commandes aux Agents (Minions). Il est possible d'avoir une infrastructure multi Master.

*Les Minions* gère les serveurs, ce sont les agents qui reçoivent et exécutent les ordres du Master. Il est possible d'avoir une infrastructure sans Master.

*Le module d'exécution*, il permet d'envoyer directement des commandes du Master au Minions, on l'utilise dans trois cas :

- Exécuter des commandes une seule fois
- Récupérer des informations en temps réel
- Exécuter des taches urgentes

Les  *States* et les *Formulas* permettent de faire la gestion de configuration sous forme de code. Ils sont écrits par défaut en YAML.

*Un State* correspond à une seule tache (Exemple: Installer une application).

*Une Formula* est un regroupement de State permettant la configuration complète de quelque chose (Un serveur LAMP et ses fichiers de configurations par exemple).

On appelle *Hightstating* le processus qui applique un State ou une Formula à des Minions.

*Les Grains* fournissent les informations à propos des systèmes sur lequel les Minions son installé.
Il permet, par exemple, de connaitre de connaitre le nom et la version de la distribution GNU/Linux d'un serveur. Ses informations peuvent être utilisée pour connaitre la famille (FreeBSD, Debian-Like, RedHat-like, etc.) et exécuter des actions appropriées pour chacun d'eux.

``` sh
$ sudo salt 'Minion1' grains.item os
Minion1:
    ------
    os:
       Ubuntu
```

*Les Pillar* sont des informations fournir par l'utilisateur et sont stocké sur le Master avant d'être assigné au Minions. Exemple de données :

- Mot de passe
- Information de configuration
- chemin de fichiers

*Les Runners* effectue des taches d'assistance sur le Master, par exemple :

- Connaitre la connectivité d'un Minion
- L'état d'un travail en cours

*Les Returners* fournissent les données en sortie vers d'autres application externe a Salt, par exemples :

- Dans une base de données
- Vers un système de monitoring
- Une application de messagerie instantanée

Un *Beacons* va de surveiller un évènement provenant d'une source externe à de Salt et le *Reactor* va agir sur l'évènement en question.


*Salt ssh* permet d'exécuter des actions sans avoir besoin de Master et de Minion.

*Salt Cloud* permet d'utiliser salt pour faire du provisioning et gérer des serveurs chez différent fournisseur Cloud.

## Installation de SaltStack

SaltStack propose trois dépôts logiciels :

- *Latest Release* qui propose la tout dernière version de SaltStack
- *Major Release* qui propose d'installer que les mises à jours mineurs
- *Minor Release* qui propose l'installation d'une version spécifique de SaltStack


L'installation de SaltStack peut se faire de deux manières, par le gestionnaire de paquet de la distribution ou via un script d'installation téléchargeable depuis le site de SaltStack.

Dans le cadre d'un lab, il est préférable de nommer le serveur Salt Master *salt* ceci afin de ne pas avoir à configurer chaque Minion durant les exercices. Il est aussi préférable d'éditer le fichier `/etc/hosts` pour y enter l'adresse IP du serveur salt, cela permet permettre au Minions de faire la résolution de nom vers le Master.

### Méthode 1 : Le gestionnaire de paquets

Dans les cas de la version sous GNU/Linux, on ajoute un dépôt externe comme indiquer sur le site officiel. Voici les liens pour les distributions :

- [OpenSUSE](https://repo.saltstack.com/#suse)
- [Debian](https://repo.saltstack.com/#debian)
- [CentOS](https://repo.saltstack.com/#rhel)

Suite a quoi nous installons les paquets *salt-master* et *salt-minion*

### Méthode 2 : Bootstrap

Sur toutes les machines il est nécessaire de télécharger le script tel que :

```bash
$ curl -L https://bootstrap.saltstack.com -o install_salt.sh
```

L'exécution du script diffère en fonction du Master et des Minions, pour le Salt Master :

```bash
$ sudo sh install_salt.sh -P -M
```

Pour les Minions :

```bash
 sudo sh install_salt.sh -P
 ```

## Configuration

### La gestion des clefs
Les communications Master/Minions de SaltStack sont basées sur du chiffrement AES tel que :

1. Les Minions cherche le serveur enregistrer dans leurs configurations (Par défaut salt). 
2. Les Minions envoie une demande et attend que l'on valide sa clef
3. L'administrateur vérifie et accepte la clef (Cette partie est automatisable).
4. Maintenant chaque Minions sont dans la capacité de recevoir des commandes du Master.

Bonus : pour chaque communication Master vers un Minion, la clef de chiffrement est différente.

Pour gérer les clefs, on utilise la commande `salt-key`, `salt-key -L` permet de voir toutes les machines qui sont ou non validée par l'administrateur ou en attente de validation. Pour permettre à un Minion de communiquer avec le Master, on doit ajouter la clef publique du Master dans le fichier de configuration des Minions.
Pour cela commence par la commande `sudo salt-key -F master` qui va nous permettre d'afficher la clef :

```sh
$ sudo salt-key -F master
Local Keys:
master.pem:  34:5e:3f:b6:0e:0f:b3:b6:ee:0c:d8:2f:95:c8:2f:b2:ff:5d:5f:f0:8f:c5:cb:87:63:ab:26:f7:aa:3c:b6:d5
master.pub:  e1:1a:8a:89:40:fb:42:12:cc:f5:fb:89:82:94:43:5a:68:5a:dd:b3:1d:92:df:72:ff:a8:83:41:cd:6f:c0:85
Unaccepted Keys:
minion1:  44:a1:c8:37:6e:b6:07:0d:ae:e1:b8:48:e8:1d:d9:3b:76:3a:c0:ed:5c:9d:50:c7:a6:a6:de:13:12:b5:b0:1c
minion2:  27:1b:a5:ef:5f:91:33:d2:52:80:80:c2:e4:13:ff:9b:9c:b5:f6:fe:f3:01:79:5b:38:17:12:f7:de:35:a0:76
salt:  bd:bd:04:43:61:69:27:21:44:1c:9d:00:64:98:c9:35:f0:2e:e5:7a:7c:c4:33:8d:45:dd:7f:23:7c:43:10:90
```
On édite le fichier de configuration des Minions `/etc/salt/minion`, on décommante l'option `master_finger:` et on y ajoute en valeur la clef publique de master :

```sh
# The state_output_profile setting changes whether profile information
# will be shown for each state run.
#state_output_profile: True

# Fingerprint of the master public key to validate the identity of your Salt master
# before the initial key exchange. The master fingerprint can be found by running
# "salt-key -f master.pub" on the Salt master.
#master_finger: ''
master_finger: 'e1:1a:8a:89:40:fb:42:12:cc:f5:fb:89:82:94:43:5a:68:5a:dd:b3:1d:92:df:72:ff:a8:83:41:cd:6f:c0:85'

# Use TLS/SSL encrypted connection between master and minion.
# Can be set to a dictionary containing keyword arguments corresponding to Python's
# 'ssl.wrap_socket' method.
# Default is None.
#ssl:
```
Après modifications sur les machines le service *salt-minion* doit être redémarré :

```sh
sudo systemctl restart salt-minion
```

On peut vérifier localement la clef d'un Minion avec la commande `salt-call --local key.finger` et la comparer avec les valeurs que l'on trouve sur le master avec `salt-key -L`.

```sh
$ sudo salt-call --local key.finger
local:
    bd:bd:04:43:61:69:27:21:44:1c:9d:00:64:98:c9:35:f0:2e:e5:7a:7c:c4:33:8d:45:dd:7f:23:7c:43:10:90
```

Si toutes les valeurs correspondent on peut maintenant accepter les clefs avec la commande suivante :

```sh
# Attention cette commande accepte toutes les clefs des Minions sans exceptions
$ sudo salt-key -A
```
### configuration du Master

Le fichier de configuration du Master se trouve a `/etc/salt/master` on peut fragmenter le fichier de configuration dans `/etc/salt/master.d/*.conf`.

Par exemple pour définir le chemin vers les Pillar et le Formula on crée un fichier `/etc/salt/master.d/file-roots.conf` :

```yaml
file_roots:
  base:
    - /srv/salt
```

Après chaque modification il est nécessaire de redémarrer le service comme suis : 

```sh
$ sudo systemctl restart salt-master
```

### Configuration des Minions

Le fichier de configuration des Minions se trouve a `/etc/salt/minion` on peut fragmenter le fichier de configuration dans `/etc/salt/minion.d/*.conf`. Il est possible de configurer les Minions de sorte a ce qu'il n'est pas besoin de Master.

Par exemple en éditant `/etc/salt/minion.d/grains.conf on peut définir le rôle de la machine.

```sh
grains:
  roles:
    - master
```

Exemple 2:

```sh
grains:
  roles:
    - prod
    - nextcloud
```

Exemple 3 :

```sh
grains:
  roles:
    - dev
    - webserver
    - database
```


### Salt Mine

Les Salt Mine sont des données équivalentes aux Grains à la différence que les données sont actualisées plus souvent (par défaut : 60 minutes). Les mines sont définies a la main et envoyer au Master, on définit les grains de deux manières :

- En créant un fichier de configuration dédier, par exemple : `/etc/salt/minion.d/mine.conf`
- En le stockant dans un Pillar au format `.sls` (Les Pillars sont vues plus bas)

Les Minions envoie ses données au Master, tous les Minions ont accès à ses données via le Master.
Uniquement les données les plus récentes sont utilisées dans les Mine.

Pour forcer la mise à jour de Salt Mine, on utilise la commande suivante :

```sh
$ sudo satl '*' mine.update
```


### Configuration Multi-Master

Il est courant de trouver une configuration Multi-Master, de cette manière il est possible de distribuer la charge et de créée une redondance entrer les Master. Il est possible de faire du FailOver, c'est-à-dire qu'un seul serveur est actif et que le second reprend le relais uniquement si le premier rencontre un problème.

Dans cette configuration tous les Master peuvent envoyer des commandes a tous les Minions. Les Minions doivent être configuré pour utiliser les deux Master. Les Master doivent avoir les même clefs  publiques et privées, la localisation de ses dernières est `/etc/salt/pki/master/` (La synchronisation du dossier est à proscrire pour des raisons de sécurités. Il est nécessaire de partager le même dossier pour les states, formula et Pillar (En général on utilise un dépôt Git).

Il est nécessaire d'appliquer ce qui suit aux clefs copiées sur les autres Master :

```sh
$ sudo chmod 0400 /etc/salt/pki/master/master.pem
$ sudo chmod 0644 /etc/salt/pki/master/master.pub
```

Le Minions doit pouvoir faire la résolution de nom vers les deux Master, pour cela on édit le fichier `/etc/salt/minion/minion` :
```yaml
master:
  - salt1
  - salt2
```

Dans tous les cas, après modifications d'un fichier de configuration on redémarre le service adéquat.

```sh
# On peut maintenant vérifier la tentative de connexion du minion :
sudo salt-key -l unacc
# on vérifie la clef du dit Minion
sudo salt-key -f minion1
# on accepte la clef de minion1 après vérification
sudo salt-key -a minion1
```

WIP: configuration d'un environnement multi-master nécessaire pour vailder le tutoriel

### Configuration MasterLess

L'utilisation du Salt Minion sans Master est possible voir pratique pour faire des tests. Il est nécessaire de stopper et désactiver au démarrage le service salt-minion. pour rendre les modifications effectives, il faut modifier le fichier `/etc/sat/minion`

```yaml
file_client: local
file_roots: /srv/salt
```
Il est absolument nécessaire de laisser le service éteint pour pouvoir fonctionner sans Master.

### Conseils de sécurités

Dans les faits, Salt a un contrôle total aux droits root sur tout les serveurs. Il est donc recommandé de limiter les accès au Salt Master, un mot de passe fort changer régulièrement, et des règles de par-feu limitant l'ouverture des ports au 4505, 4506 et ssh. Les Formulas et les States sont sur un Git non publique et revus de code sont obligatoires.

Il est recommandé pour les Minions de limiter les accès au strict nécessaire et d'utiliser les Beacon et les Reactor autant que possible.

Il est possible de désactiver des modules en créant un fichier de conf par exemple :

`$ sudo vim /etc/salt/minion.d/disable.conf`

```yaml
disable_modules:
  - cmdmod # voir https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.cmdmod.html
  - network # voir https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.network.html
```

## Les Modules d'exécution

Les modules d'exécution permettent d'exécuter des commandes sur plusieurs serveurs en une fois depuis le master. 

Les modules d'exécutions tendent à être agnostique du système d'exploitation, mais il reste quelques modules où cela ne s'applique pas.

On appelle une fonction tel que `module.fontion`, par exemple :

```sh
# sudo salt <cible> <module>.<fonction> <parametre>
```

l'exemple suivant utilise le module *pkg* et sa fonction *install* pour installer htop sur tous les serveurs.

```sh
sudo salt '*' pkg.install htop
```

Comment cela fonctionne:

1. l'utilisateur lance la commande salt
2. l'utilisateur est authentifié
3. la commande est placer en file d'attente
4. le Master publie la commande
5. le Minion reçoit la commande
6. le Minion exécute la commande
7. le résultat de la commande est formaté
8. Le résultat de la commande est envoyée sur le bus des évènements
9. le résultat est envoyé au Master

### Le ciblage par filtre

Il est possible de cibler des machines de façons très fine avec StaltStack, pour cela nous pouvons utiliser des caractères de Gobbling (*, ?, [], des expressions régulières et des listes. Cela nous permettra de chercher une cible en fonction de :

- du nom du Minion
- des Grains (Type d'OS, Nom de la machine, etc ...)
- du nom d'un groupe de nœud
- des données Pillar
- du sous-réseau et de l'adresse IP

exemples :

```sh
# Exécute un ping sur tout les Minions dont le nom commence par minion quelle que soit la suite du nom
sudo salt 'minion*' test.ping

# Exécute un ping sur tout les Minions dont le nom commence par minion en ce limitant à un caractère supplémentaire
sudo salt 'minion?' test.ping

# Exécute un ping sur tout les minion dont le nom commence par minion dont la fin du nom est compris de 1 a 5 :
sudo salt 'minion[1-5]' test.ping

# Exécute un ping sur le master et minion2
sudo salt -L 'salt,minion2' test.ping

# Exécute un ping sur tout les machines utilisant la distribution Ubuntu en ce basant sur les informations fournis par les Grains
sudo salt -G 'os:Ubuntu' test.ping

# Exécute un ping sur tout les machines ayant e role de serveur web en ce basant sur les informations fournis par les Grains
sudo salt -G 'roles:webserver' test.ping

# Exécute un ping sur tout les minion via l'adresse IP 
sudo salt -S 10.0.0.1 test.ping
```

Il est possible de faire des groupes de serveurs en éditant un fichier de configuration du master, par exemple en créant et éditant un fichier dans `/etc/salt/master.d/nodegroups.conf` :

```yaml
nodegroups:
  centos-dev: 'G@os:CentOS and G@roles:dev'
```

Le ciblage ce fait de façons suivante :

```
sudo salt -N centos-dev test.ping

Ce bout de code nous montre comment créer un groupe de machines. L'utilisation de *G@* permet de limiter la recherche dont l'OS est CentOS *et* dont le rôle est dev via les informations fournis par les grains. Le résultat des filtres servent à définir le groupe centos-dev.

Après modification d'un élément dans `/etc/salt/master.d` il est nécessaire de redémarrer le service du Master :

```sh
sudo systemctl restart salt-master
```
Pour avoir un filtre similaire on utilise l'option `-C` tel que :

```sh
sudo salt -C 'G@os:CentOS and G@roles:dev'
```

Il est possible de faire appel au groupe pour cibler l'exécution de commande :

```sh
sudo salt -N centos-dev test.ping
```

Pour exécuter les actions sur plusieurs Minion simultanément on utilise l'option batch `-b` dont la valeur définie le nombre d'action simultanée.

```sh
sudo salt '*' -b 2 test.ping
```

Pour plus d'information sur les filtres utilisables voir la page suivante :
[SaltStack : Compound matchers](https://docs.saltstack.com/en/latest/topics/targeting/compound.html#targeting-compound)

### Salt-Call

La commande `salt-call` est l'équivalent local de `salt`, elle n'est utilisée que depuis un Minion.

```sh
sudo salt-call --local pkg.list_upgrades refresh=True
```

L'utilisation de l'option `--local` indique que l'on souhaite travailler localement, on peut aussi observer que l'on peut ajouter des options aux modules utilisés.

`salt-call` peut-être fortement utile dans le cas d'un développement d'un module ou d'un state, cela permet d'obtenir plus d'informations et des logs plus détaillés.

## Les modules communs

### Le module sys

Le module *sys* est un module permettant d'obtenir des informations sur les modules, leurs fonctions et leurs arguments.

Par exemple pour obtenir la liste des fonctions du module *pkg* :

```sh
$ sudo salt 'minion*' sys.list_functions pkg
minion1:
    - pkg.audit
    - pkg.autoremove
    - pkg.available_version
    - pkg.backup
    - pkg.check
    - pkg.clean
    - pkg.delete
    - pkg.fetch
    - pkg.hold
    - pkg.info
    - pkg.install
    - pkg.latest_version
    - pkg.list_locked
    - pkg.list_pkgs
    - pkg.list_upgrades
    - pkg.lock
    - pkg.locked
    - pkg.parse_config
    - pkg.purge
    - pkg.refresh_db
    - pkg.remove
    - pkg.restore
    - pkg.search
    - pkg.stats
    - pkg.unhold
    - pkg.unlock
    - pkg.update
    - pkg.update_package_site
    - pkg.updating
    - pkg.upgrade
    - pkg.version
    - pkg.version_cmp
    - pkg.which
```

Pour obtenir la liste des arguments de la fonction *pkg* ainsi que les paramètres par défaut :

```sh
$ sudo salt 'minion*' sys.argspec pkg.install
minion1:
    ----------
    pkg.install:
        ----------
        args:
            - name
            - fromrepo
            - pkgs
            - sources
            - jail
            - chroot
            - root
            - orphan
            - force
            - glob
            - local
            - dryrun
            - quiet
            - reinstall_requires
            - regex
            - pcre
            - batch
        defaults:
            - None
            - None
            - None
            - None
            - None
            - None
            - None
            - False
            - False
            - False
            - False
            - False
            - False
            - False
            - False
            - False
            - False
        kwargs:
            True
        varargs:
            None
```

Pour chaque argument afficher correspond une valeur par défaut, la valeur par défaut de *name* est *none* la valeurs par défaut de *batch* est False.

La liste des fonctions et des arguments avec des commentaires détaillés sont obtenus avec l'argument doc de la fonction *sys*:

Par exemple obtenir la documentation de l'argument *install* de la fonction *pkg* :
```sh
$ sudo salt 'minion*' sys.doc pkg.install
pkg.install:

    Install package(s) from a repository

    name
        The name of the package to install

        CLI Example:

            salt '*' pkg.install <package name>

    jail
        Install the package into the specified jail

    chroot
        Install the package into the specified chroot (ignored if ``jail`` is
        specified)

    root
        Install the package into the specified root (ignored if ``jail`` is
        specified)
....

....
    regex
        Treat the package names as a regular expression

        CLI Example:

            salt '*' pkg.install <regular expression> regex=True

    pcre
        Treat the package names as extended regular expressions.

        CLI Example:


    batch
        Use BATCH=true for pkg install, skipping all questions.
        Be careful when using in production.

        CLI Example:

            salt '*' pkg.install <package name> batch=True
```

À savoir que l'argument est optionnel il est tout à fait possible d'utiliser *sys.doc* sur lui meme :

```sh
$ sudo salt 'minion*' sys.doc sys
ys.argspec:                                                                                [345/1966]

    Return the argument specification of functions in Salt execution
    modules.

    CLI Example:

        salt '*' sys.argspec pkg.install
        salt '*' sys.argspec sys
        salt '*' sys.argspec

    Module names can be specified as globs.

    New in version 2015.5.0

        salt '*' sys.argspec 'pkg.*'



sys.doc:

    Return the docstrings for all modules. Optionally, specify a module or a
    function to narrow the selection.

    The strings are aggregated into a single document on the master for easy
    reading.

    Multiple modules/functions can be specified.

    CLI Example:

        salt '*' sys.doc
        salt '*' sys.doc sys
        salt '*' sys.doc sys.doc
        salt '*' sys.doc network.traceroute user.info

    Modules can be specified as globs.

    New in version 2015.5.0

        salt '*' sys.doc 'sys.*'
        salt '*' sys.doc 'sys.list_*'


sys.list_functions:

    List the functions for all modules. Optionally, specify a module or modules
    from which to list.

sys.list_functions:

    List the functions for all modules. Optionally, specify a module or modules
    from which to list.

    CLI Example:

        salt '*' sys.list_functions
        salt '*' sys.list_functions sys
        salt '*' sys.list_functions sys user

    Function names can be specified as globs.

    New in version 2015.5.0

        salt '*' sys.list_functions 'sys.list_*'

    New in version ?

        salt '*' sys.list_functions 'module.specific_function'
...

...
sys.state_doc:

    Return the docstrings for all states. Optionally, specify a state or a
    function to narrow the selection.

    The strings are aggregated into a single document on the master for easy
    reading.

    Multiple states/functions can be specified.

    New in version 2014.7.0

    CLI Example:

        salt '*' sys.state_doc
        salt '*' sys.state_doc service
        salt '*' sys.state_doc service.running
        salt '*' sys.state_doc service.running ipables.append

    State names can be specified as globs.

    New in version 2015.5.0

        salt '*' sys.state_doc 'service.*' 'iptables.*'



sys.state_schema:

    Return a JSON Schema for the given state function(s)

    New in version 2016.3.0

    CLI Example:

        salt '*' sys.state_schema
        salt '*' sys.state_schema pkg.installed
```

### le module test

Le module *test* permet de faire des tests variés sur salt, comme faire un ping sur tout les Minions.

La liste des fonctions permet d'avoir un aperçu sur les possibilités du module *test* :

```sh
sudo salt 'salt*' sys.list_functions test

salt:
    - test.arg
    - test.arg_clean
    - test.arg_repr
    - test.arg_type
    - test.assertion
    - test.attr_call
    - test.collatz
    - test.conf_test
    - test.cross_test
    - test.echo
    - test.exception
    - test.false
    - test.fib
    - test.get_opts
    - test.kwarg
    - test.module_report
    - test.not_loaded
    - test.opts_pkg
    - test.outputter
    - test.ping
    - test.provider
    - test.providers
    - test.raise_exception
    - test.rand_sleep
    - test.rand_str
    - test.random_hash
    - test.retcode
    - test.sleep
    - test.stack
    - test.true
    - test.try
    - test.tty
    - test.version
    - test.versions
    - test.versions_information
    - test.versions_report
```

### Le module pkg

Le module *pkg* nous permet de travailler avec les paquets logiciels de nos serveurs. Le module pkg est un module dit *virtuel*, c'est-à-dire qu'il combine les fonctionnalités de nombreux modules orienté sur un système ou une distribution spécifique .

Pour plus d'informations sur les modules pris en charge avec pkg voir ce [liens](https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.pkg.html).

Par exemple l'installation d'un paquet ce fait avec l'argument *install* de la fonction *pkg* :

```sh
$ sudo salt '*' pkg.install irssi

minion1:
    ----------
    irssi:
        ----------
        new:
            1.2.1_1,1
        old:
salt:
    ----------
    irssi:
        ----------
        new:
            1.2.1_1,1
        old:
```

Pour supprimer un paquet on utilise l'argument *remove* de la fonction *pkg* :

```sh
$ sudo salt '*' pkg.remove irssi

minion1:
    ----------
    irssi:
        ----------
        new:
        old:
            ----------
            origin:
                irc/irssi
            version:
                1.2.1_1,1
salt:
    ----------
    irssi:
        ----------
        new:
        old:
            ----------
            origin:
                irc/irssi
            version:
                1.2.1_1,1
```

Pour mettre à jour son système on utilise l'argument *upgrade* de la fonction *pkg* et ainsi de suite. Il est conseiller de regarder la documentation du module *sys* pour plus d'informations.

### Les modules user et group
Pour la gestion des groupes et des utilisateurs, il existe respectivement les modules virtuels *group* et  *user*.

Pour créer un utilisateur on utilise *add.user* :

```sh
$ sudo salt 'salt*' user.add rollniak home=/home/rollniak groups=sudo

salt:
    True
```sh
Pour ajouter un mot de passe, on utilise la fonction *gen_password* :

$ sudo salt 'salt' shadow.gen_password 'password'
salt:
    $6$j9nBtTFy$v2cKaBJXNqWsDCVFD886gtF/GdCkxdZZv2wa1n01sDpzA0WCV3eANfk5ys/MsNuAMqO0qgXQ0fhbUuKlh3wcW/
```

Il peut arriver que *gen_password* soit indisponible pour le système. Dans ce cas, il est nécessaire de se référer a la documentation de *shadow.set_password* :

```sh
cloud_user@rollniak1c:~$ sudo salt 'salt' sys.doc shadow.set_password
shadow.set_password:

    Set the password for a named user. The password must be a properly defined
    hash. The password hash can be generated with this command:

    ``python -c "import crypt; print crypt.crypt('password',
    '\$6\$SALTsalt')"``

    ``SALTsalt`` is the 8-character crpytographic salt. Valid characters in the
    salt are ``.``, ``/``, and any alphanumeric character.

    Keep in mind that the $6 represents a sha512 hash, if your OS is using a
    different hashing algorithm this needs to be changed accordingly

    CLI Example:

        salt '*' shadow.set_password root '$1$UYCIxa628.9qXjpQCjM4a..'
```

Pour appliquer le mot de passer hasher à un utilisateur, on utilise la fonction *shadow.set_password :

```sh
$ sudo salt 'salt' shadow.set_password rollniak '$6$j9nBtTFy$v2cKaBJXNqWsDCVFD886gtF/GdCkxdZZv2wa1n01sDpzA0WCV3eANfk5ys/MsNuAMqO0qgXQ0fhbUuKlh3wcW/'

salt:
    True
```

Pour créer un groupe, on utilise le module *group* avec la fonction *add* :

```sh
$ sudo salt 'salt' group.add salt
salt:
    True
```

Pour ajouter un utilisateur à un groupe on utilise la fonction *adduser* du module *group* :

```sh
udo salt 'salt' group.adduser salt rollniak
salt:
    True
```

### Le module grains

Le module grains permet de récupérer des informations sur la machine. Il permet aussi d'en définir de nouvelles.

Précédemment, l'utilisation de grains.ls a permis de lister les type de grains disponible :

```sh
$ sudo salt 'minion1' grains.ls
minion1:
    - SSDs
    - biosreleasedate
    - biosversion
    - cpu_flags
    - cpu_model
    - cpuarch
    - disks
    - dns
    - domain
    - fqdn
    - fqdn_ip4
    - fqdn_ip6
    - fqdns
    - gid
    - gpus
    - groupname
    - host
    - hwaddr_interfaces
    - id
    - init
    - ip4_gw
    - ip4_interfaces
    - ip6_gw
    - ip6_interfaces
    - ip_gw
    - ip_interfaces
    - ipv4
    - ipv6
    - kernel
    - kernelrelease
    - kernelversion
    - locale_info
    - localhost
    - lsb_distrib_codename
    - lsb_distrib_description
    - lsb_distrib_id
    - lsb_distrib_release
    - machine_id
    - manufacturer
    - master
    - mdadm
    - mem_total
    - nodename
    - num_cpus
    - num_gpus
    - os
    - os_family
    - osarch
    - oscodename
    - osfinger
    - osfullname
    - osmajorrelease
    - osrelease
    - osrelease_info
    - path
    - pid
    - productname
    - ps
    - pythonexecutable
    - pythonpath
    - pythonversion
    - roles
    - saltpath
    - saltversion
    - saltversioninfo
    - serialnumber
    - server_id
    - shell
    - swap_total
    - systemd
    - uid
    - username
    - uuid
    - virtual
    - zfs_feature_flags
    - zfs_support
    - zmqversion
```

Il est possible de voir le contenu des *grains* avec deux fonctions :

- grains.fetch
- grains.item

grains.item fait la même chose à ceci prêt que grains.item affiche une information supplementaire.

```sh
$ sudo salt 'minion1' grains.fetch roles
minion1:
    - dev
    - webserver
    - database

$ sudo salt 'minion1' grains.item roles
minion1:
    ----------
    roles:
        - dev
        - webserver
        - database
```

Pour afficher le contenu de tout les item en une seule fois, on utilise *grains.items* sans argument.

On utilise *grains.set* pour ajouter des grains :

```sh
$ sudo salt 'minion1' grains.set 'sites' eski
minion1:
    ----------
    changes:
        ----------
        sites:
            eski
    comment:
    result:
        True
```

Ci-dessous on ajoute un nouveau grains pour *sites* :

```sh
sudo salt 'minion1' grains.append sites internal convert=True
minion1:
    ----------
    sites:
        - eski
        - internal
```

L'option *convert* permet de convertir le contenu sous forme de liste. 

```sh
$ sudo salt 'minion1' sys.doc grains.append
grains.append:

    New in version 0.17.0

    Append a value to a list in the grains config file. If the grain doesn't
    exist, the grain key is added and the value is appended to the new grain
    as a list item.

    key
        The grain key to be appended to

    val
        The value to append to the grain key

    convert
        If convert is True, convert non-list contents into a list.
        If convert is False and the grain contains non-list contents, an error
        is given. Defaults to False.

    delimiter
        The key can be a nested dict key. Use this parameter to
        specify the delimiter you use, instead of the default ``:``.
        You can now append values to a list in nested dictionary grains. If the
        list doesn't exist at this level, it will be created.

        New in version 2014.7.6

    CLI Example:

        salt '*' grains.append key val
```

Pour supprimer du contenu dans les grains on utilise *grains.remove*

```sh
$ sudo salt 'minion1' grains.remove sites internal
minion1:
    ----------
    sites:
        - eski
```

Pour supprimer un grains, on supprime la clef correspondante :

```sh
$ sudo salt 'minion1' grains.delkey sites
minion1:
    None

$ sudo salt 'minion1' grains.get sites
minion1:
```

L'observation de la seconde commande confirme la disparition du grains.

### Le module cmd

Le module *cmd* permet de lancer des commandes du système cible, cela implique de prendre en compte les différences entre chaque système avant de lancer une commande.

Exemple :

```sh
$ sudo salt '*' cmd.run 'cat /etc/hosts'
salt:
    127.0.0.1 localhost salt

    # The following lines are desirable for IPv6 capable hosts
    ::1 ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    ff02::3 ip6-allhosts
    # Cloud Server Hostname mapping
    52.215.13.159 rollniak1c.mylabserver.com
    172.31.104.38       salt
    172.31.106.137      minion1
    172.31.107.16       minion2
minion2:
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 minion2
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    # Cloud Server Hostname mapping
    52.215.222.166 rollniak3c.mylabserver.com
    172.31.104.38   salt
    172.31.106.137  minion1
    172.31.107.16   minion2
minion1:
    127.0.0.1 localhost minion1

    # The following lines are desirable for IPv6 capable hosts
    ::1 ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    ff02::3 ip6-allhosts
    # Cloud Server Hostname mapping
    52.51.122.94 rollniak2c.mylabserver.com
    172.31.104.38   salt
    172.31.106.137  minion1
    172.31.107.16   minion2
```

! Il est important d'utiliser l'option *runas* quand cela est possible, sans cela tout les commande sont exécutées en **root**.

pour plus d'informations regarder les détails du module avec *sys*


### Le module Git

Le module *git* permet a SaltStack de travailler avec Git.

La première chose à faire et de configurer Git sur la machine cible :

```sh
$ sudo salt 'salt' git.config_set user.name rollniak global=True user=user
$ sudo salt 'salt' git.config_set user.email rollniak@test.test global=True user=user
```

La création et la configuration du répertoire accueillant les futurs formula :

```sh
$ sudo mkdir /srv/salt
$ sudo chgrp salt /srv/salt/
$ sudo chmod 775 /srv/salt/
$ sudo chmod g+s /srv/salt/
```

On initialise le dépôt git local :

```sh
$ sudo salt 'salt' git.init cwd=/srv/salt/mysql user=user
[sudo] password for user:
salt:
    Initialized empty Git repository in /srv/salt/mysql/.git/
```

On définit le répertoire distant pour le dépôt git local :

```sh
$ sudo salt 'salt' git.remote_set /srv/salt/mysql https://github.com/elle-la/la-salt-mysql.git
salt:
    ----------
    fetch:
        https://framagit.org/rollniak/la-mysql-formula
    push:
        https://framagit.org/rollniak/la-mysql-formula
```

## States et Formula

### State

Les states son des fichiers unique au format YAML avec pour extension `.sls`.
Un state est défini comme suis :

```YAML
nom_du_state:
  module.fonction:
    - option1: valeur1
    - option2: valeur2
    - option3: valeur3
    - option4: valeur4
```

Pour utilise un state on utilise la commande suivante :

```sh
$ sudo salt 'salt' state.sls nom_du_fichier test=True
```

Deux precision :

- le nom du fichier ne contient pas l'extension ".sls"
- l'utilisation de test=True permet d'exécuter le state sans appliquer de modification.

Rappel : Dans le cas de ce guide, le dossier racine des state et des formula se situe dans `/srv/salt`.
Si le fichier state se trouve dans `/srv/salt/firststatedir/state.sls`, alors il faudra entrer la commande de la manière suivante :

```sh
$ sudo salt 'salt' state.sls nom_du_dossier.nom_du_fichier test=True
```

La copie d'un fichier ce fait via le module l'exécution *cp* :

```sh
$ sudo salt 'salt' cp.get_file salt://fichier_a_la_racine_du_dossier_des_states destination
```

Dans un fichier Yaml:

```YAML
nom_de_laction:
  file.managed:
    - name: chemin/de/destination
    - source: salt://chemin_relatif_depuis_racine_de_salt
```

#### prérequis:

Il est possible de définir des dépendances entre les states avec des prérequis (requisites).

Les requisites fonctionnent de deux manières :

- avec *require* qui définie la dépendance d'une state
- avec *require_in* qui définie qu'elles sont les state qui en dépendes

Par exemples state_config.sls nécessite d'avoir paquet A installer :

```YAML
config_service:
  file.managed:
    - name: chemin/de/destination
    - source: salt://chemin_relatif_depuis_racine_de_salt
    - require:
      - pkg: paquet_A
```

Si on exécute la state seul alors que paquet_A n'est pas installé :

```sh
$ sudo salt '*' state.sls state_install,state_config test=true
```

La commande retourne une erreur, car les prérequis ne sont pas satisfaits. Pour installer paquet_A on a besoin de la state_install.sls :

```yaml
install_paquet_A:
  pkg.installed:
    - name: paquet_A
```

Pour exécuter plusieurs states les un à la suite des autres, on utilise la commande salt comme suit :

```sh
$ sudo salt '*' state.sls state_install,state_config test=true
```

Il existe plusieurs types de prérequis:

- require: nécessite que les state cible se terminent avec succès quelle que soit la valeur retourner (True, False, etc...) afin d'être exécuté
- watch: Exécute la state uniquement dans le cas où les dépendances retourne une valeur Vrai (True)
- prereq: Exécute la dépendance en mode test=true, si la dépendance effectue des changements, la state actuelle s'exécutera avant
- onchanges: Ne s'exécute que si la dépendance à effectuer des changements
- onfail: la state est prise en compte uniquement si l'exécution de la dépendance échoue

Si l'on souhaite redémarrer un service suite a la modification d'un fichier :

```yaml
service_restart:
  module.wait: # ce module s'exécute uniquement sous certaine condition
    - name: service.restart # on place le nom du module et sa fonction ici
    - m_name: service_A # on place la cible de la fonction
    - watch:
      - config_service # correspond au nom de state_config.sls
```

Comme précédemment il est possible de faire les tests en ajoute les state les un a la suite des autres :

```sh
$ sudo salt '*' state.sls state_install,state_config,state_restart test=true
```

#### init.sls

Jusqu'à présent on a lancé chaque state manuellement. Pour faire en sorte que le dossier soit considéré comme une formula, il sera nécessaire d'y intégrer le fichier init.sls. Ce fichier inclue tous les states qui vont être exécuter.

```yaml
include:
  - formula.state_install
  - formula.state_config
```

Pour faire appel a une formula on entre le nom du dossier qui contient l'arborescence et les state de la formula.

```sh
sudo salt '*' state.sls dossier_formula test=true
```

Il est conseiller d'inclure les state de redémarrage directement dans la state qui nécessite un reboot, dans notre cas state_config.

```YAML
include:
  - dossier_formula.state_restart

config_service:
  file.managed:
    - name: chemin/de/destination
    - source: salt://chemin_relatif_depuis_racine_de_salt
    - require:
      - pkg: paquet_A
```

L'inclusion de fichier doit toujours apparaitre en haut du fichier.

#### top.sls

Le fichier top.sls permet de définir quel state/formula sera envoyé à quel Minion, pour cela on utilise les filtre suivant dans le fichier :

- Le nom du/des minion(s) avec ou sans expression régulière
- Les Grains
- les Pillar
- les sous-réseaux
- les datastore
- les groupe de nœuds

Le fichier top.sls commence par *base:*

Exemple de fichier top.sls:

```yaml
base:
  'G@roles:database': # Recherche dans les grains le role database est applique la formula
    - mysql
  'G@roles:dbclient'
    - mysql.client # Utilise uniquement le state client.sls dans la furmula mysql
```

Dans le cas où un minion se trouvera sans tâches, le highstate relavera une erreur avec la commande suivante :

```sh
$ sudo salt '*' state.highstate test=true
```

### Jinja

Jinja est moteur de template pour python utiliser par défaut dans SaltStack. Il permet d'adapter la configuration de nos fichiers sur une infrastructure hétérogène.

```jinja
{% ... %} # est utilisé pour les déclarations
{{ ... }} # est utilisé pour référence des variables dans SaltStack
{# ... #} # est utilisé pour écrire des commentaires
```

Par exemple pour exécuter des actions ciblant uniquement les serveurs utilisant le système d'exploitation basé Debian :

``` Jinja
{% if grains['os_family'] == 'Debian' %}

{% endif %}
```

Attention toutefois aux notes sur le site de SaltStack, cela évitera quelques incomprehension (Example : https://docs.saltstack.com/en/latest/ref/states/all/salt.states.debconfmod.html)

Quelques exemples d'utilisation de Jinja sont disponibles sur le site de SaltStack : [Understanding Jinja](https://docs.saltstack.com/en/latest/topics/jinja/index.html)

#### map.jinja

Pour rendre une formula multi plateforme, on utilise le fichier `map.jinja`. Ce fichier contient en différentes informations sous forme de template Jinja.
Par exemple on peut y définir les différents noms de paquet pour chaque distribution ainsi les emplacements des fichiers de configuration adapter a chaque OS.

Exemple :

```jinja
{% set service = salt['grains.filter_by']({


  'Debian': {
    'server': 'nom_paquet_sur_debian_ubuntu',
  },
  'RedHat': {
    'server': 'nom_paquet_sur_redhat_centos',
  },

})%}
```

La première ligne donne le nom *service* au set de filtre qui sera appelé dans un state. On appelle la fonction `grains.filter_by` de salt qui filtre les OS par défaut.

Ensuite on assigne à la variable *server* le nom du paquet que l'on souhaite installer en l'adaptant a la famille du système d'exploitation cible.

On édite le début du fichier state_install de la manière qui suis :


```yaml
{% from "service/map.jinja" import service with context %}

service_server_install:
  pkg.installed:
    - name: {{ service.server }}
```

La première ligne fait appel a la formula du service et importe le set pour installer le programme demander en fonction de la distribution.
