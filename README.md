# Personnal wiki

A wikinotes of my studies, reading and discoveries through  internet.

## Editors

- [vim/neovim](editors/vim/index.md)

## Configuration Management Tools

- [SaltStack](cfgmgmt/saltstack/index.md)

## Source Code Managers

- [Git](scm/git/index.md)
