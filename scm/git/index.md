# Git

## Introduction a Git et son architecture.

Git a était crée par Linus Torvalds, l'outil gère des snapshots du contenu, la somme de contrôle, et les métadonnées pour garder les traces des changements de chaque fichier.

Git est adapté à des fichiers au format texte, il n'ait pas fait pour le suivi de fichiers binaires.

### Terminologie
Qu'est-ce qu'un commit ?
- chaque état (Snapshot ?) est appelé un commit
- Le commit sur lequel on est positionnés est appelé Head
- Les commits sont affiliés à des dépôts de code source et des branches
- Le Head se déplacent à chaque nouveau commit

## Installer Git
L'installation de Git est très simple, on utilise le gestionnaire de paquet du système en cours d'utilisation :

- Debian-likes : `sudo apt-get update && sudo apt-get install git`
- RedHat-likes : `sudo yum install git`
- OpenSUSE : `sudo zypper install git`
- FreeBSD : `sudo pkg install git`

## Configurer Git

La configuration de base de Git nécessite à minima un nom/pseudo, une adresse email et un éditeur de texte (Vim, Emacs, Nano ou autre).

Configuration en espace utilisateur :

- `git config --global user.name <nom_d_utilisateur>`
- `git config --global user.email <adresse_email>`
- `git config --global core.editor <editeur_de_texte>`

Les options permettent d'appliquer la configuration a different niveau du system ou d'un depot.

- `sudo git config --system` s'applique au système entier, la localisation du fichier git pour le système est :
  - GNU/Linux : `/etc/gitconfig`
  - FreeBSD : `/usr/local/etc/gitconfig`
- `git config --global` s'applique à l'utilisateur courant, ses valeurs écrasent les valeurs systèmes, les informations sont stockées dans `$HOME/.gitconfig` ou dans `$HOME/.config/git/config`.
- `git config --local` s'applique au dépôt dans lequel on se situe, les valeurs écrasent celles du système de l'utilisateur, les informations sont stockées dans `.git/config`.

Les fichiers de configuration de Git sont au format INI :

```ini
[user]
    name = John SMITH
    email = john@smith.test
```

Tips : `git config -l` permet de connaitre la configuration actuelle de Git. Toutefois cela ne permet pas de voir les valeurs enregistrées pour le système.

## travailler avec les dépôts Git

Pour créer un répertoire, on utilise `git init <repertoire_du_depot>`. La commande crée un répertoire `.git` utilisé pour stocker les métadonnées.

Pour indiquer à Git que l'on veut suivre un fichier, on utilise la commande `git add <fichier>`.

On peut vérifier le suivi avec `git status` pour connaitre l'état du dépôt. Après avoir ajouté des fichiers, on utilise `git commit` pour créer un commit ce qui ouvre notre éditeur de texte.

Pour arrêter le suivi, on utilise la commande `git rm <fichier>`. Pour annuler la suppression les actions suivantes sont requises :

```sh
$ git reset HEAD <file>
$ git checkout -- <file>
```

## Ignorer des fichiers

Git permet d'ignorer des fichiers pour différentes raisons, les fichiers les plus couramment ignorés sont les résultats de compilation du programme.

Pour ignorer des fichiers, on édite le fichier `.gitignore` dans le dépôt local, ou on indique le chemin vers un fichier du même type avec `git config --global core.excludesfile <path>`.

Exemple de fichier `.gitignore`

```
# Les commentaires commencent par #

# Tous les fichiers ce terminant par .ogg et .raw sont ignorés:
*.ogg
*.raw

# le dossier build/ et sont contenu et ignorer
build/
```

## Cloner un dépôt git

Pour cloner un dépôt git, on utilise la commande `git clone`, exemple :

``` sh
$ git clone <chemin d'orgine> <chemin de destination>
```

Le chemin d'origine peut être un dépôt local situé dans un entre emplacement de l'arborescence de fichier, une clé USB par exemple, il peut aussi être une adresse HTTPS. Le chemin de destination est optionnel et permet de choisir un autre nom de dossier du projet cloner.


Pour partager nos modifications, on utilise la commande `git push origin master`.

## Les logs dans Git

C'est la commande `git log` qui va nous afficher des informations sur les commits et les changements qui ont eu lieu dans le dépôt Git.
Les informations contiennent le numéro, l'auteur, la date et le message du commit.

Dans le cas où il y a trop d'informations, il est possible  d'utiliser l'option `--oneline`. Dans le cas contraire, si l'on souhaite afficher plus d'informations il est possible d'utiliser l'option `-p`. Dans tous les cas, il est possible de préciser un fichier dont on souhaite connaitre l'historique par `git log <fichier>`.

Si le projet utilise des branches, il est peut-être préférable d'utiliser les options `--decorate` et `--graph` afin d'afficher en ASCII Art la suite les modifications apportées au projet.

## Branches

Une branche est une copie d'une autre branche (En général master) à partir de laquelle on peut travailler sans interférer avec la branche principale. Une fois le travail terminé, la branche précédemment crée peut-être fusionnée avec master.

Pour créer une branche on utilise `git branch <nouvelle branche>`, on se déplace d'une branche à une autre avec `git checkout <nom de la branche>`. Il est possible de faire les deux en une commande avec `git chekout -b <nouvelle branche>`.

Pour pousser les branches sur le dépôt distant, on utilise `git push origin --all`

## Fusion de branche

Pour fusionner le travail d'une branche X avec master, on utilise la commande `git merge <branch de travail>`.
