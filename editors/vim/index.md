# Vim

## Les bases de Vim

Vim est un éditeur modal, il existe actuellement trois modes :
- mode commande
- mode insertion
- mode visuel

Le *mode commande* et le mode par défaut, pour quitter les autres monde on utilise la touche *Échappe*. Le *mode commande* permet d'utiliser les touches du clavier comme des macros pour exécuter des actions sur le texte.

Le *mode insertion* permet d'insérer du texte, on utilise la touche `i` par défaut pour entrer en mode insertion et échappe pour en sortir.

Pour le déplacement, les touches par défaut sont `h`, `j`, `k`, `l`, pour gauche, bas, haut, droite respectivement.
Pour se déplacer de mot en mot on utilise la touche `w` et `e`, `w` permet d'avancer en plaçant le curseur au début de chaque mot, `e` fait la même chose mais place le curseur en fin de mot.
Pour se déplacer d'un mot a un mot précèdent on utilise la commande `b`.

Pour revenir au début de la ligne, on utilise la touche `0` (Zéro), pour effectuer la même chose dans le sens inverse, on utilise la touche `$`.

Pour se déplacer d'un paragraphe a un autre on utilise les accolade `{` et `}` qui permettent respectivement de reculer et d'avancer d'un paragraphe a un autre.

L'utilisation de `%` permet de se déplacer d'une parenthèse a une autre ou d'une accolade a une autre. Très pratique durant l'édition d'un fichier source.

Il est possible d'ajouter du contenu après le curseur avec la lettre `a`, le `A` ajoute du contenu a la fin de la ligne.

Pour insérer du texte au niveau du curseur, on utilise la commande `i`, on utilise `I` Pour ajouter du contenu au début de la ligne.

Pour remplacer du texte peut utiliser la touche `r` pour remplacer, cela ce limite a un seul caractère. L'utilisation de `s` permet de remplacer et ajouter à la suite du contenu.

Pour la suppression de contenu, `x` permet la suppression d'un caractère. On peut utilise `dd` pour supprimer une ligne entière. Le premier *d* fonctionne toujours de paire avec une autre touche macro, par exemple `dw` supprime un mot.

Pour annuler des modifications on utilise la commande `u` et on utilise `ctrl`-`r`.

Pour réitérer l'entrer de contenu on utilise soit le point `.` ou la virgule `,`.

L'utilisation de `<nombre>I` exécute une boucle qui répètera ce qui a était écrit le nombre de fois choisi.

L'utilisation de `y<something>` est utilisé pour copier, par exemple copier un ligne entière on utilise `yy`, pour copier un mot on utilise `yw`

Le *mode de marquage* commence en utilisant la commande `v`. On entre en mode marquage, on déplace le curseur afin de sélectionner le texte et par exemple copier la sélection avec `y`.


La recherche d'un pattern ce fait avec `/` suite a quoi on cherche les itérations avec la touche `n` pour les suivant et `N` pour les précèdent.
L'utilisation de `?` permet de lancer une recherche depuis le bas de la page.

On entre `:` pour rentrer en exécution mode, par exemple `: %s/pattern/PATTERN/gc` remplace le mot pattern par PATTERN sur tout le fichier. La lettre *c* sert à demander la confirmation avant de provoquer les changements.

Après être entré en *mode exécution*, si on débute la commande par `!`, on peut lancer des commandes externe, par exemple `:! gcc -o main main.c`.

Il est possible de lire le contenu d'un fichier et d'intégrer directement dans le fichier en cours de rédaction avec `:r ! cat <chemin du fichier>`.


Il est possible d'exécuter des commandes sur un nombre de ligne limite, par exemple `: 16:23 ! sort -r` aura pour effet de trier dans le sens inverse le contenu de la ligne 16 a 23.

Pour enregistrer le contenu et quitter on utilise `:wq`, il est possible de quitter sans enregistrer avec `:q!` on peut aussi quitter avec `ZZ`.

On peut sauvegarder sous avec `:saveas <chemain/nom_du_fichier>.

Pour passer d'un buffer à un autre on utilise `:bn` ou `CTRL+6`, pour aller au précédent on utilise `:bp`. Dans le cas d'une fenêtre diviser en deux on utilise le `CTRL+w` puis l'une des touches de déplacement
